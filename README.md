# iCalMerge

iCalMerge is a JavaScript class that enables the merging of two iCal files (.ics) together. This can be useful when you have two separate calendars and want to consolidate the events into a single calendar.

## Installation

You can install iCalMerge as an npm package using the following command:

```bash
npm install icalmerge
```

## Usage

To use iCalMerge, you need to require the module and create an instance of the `iCalMerge` class, passing the two iCal files that you want to merge as parameters.

```javascript
const iCalMerge = require('icalmerge');

const old_iCalContent = `...`; // The content of the first iCal file
const new_iCalContent = `...`; // The content of the second iCal file

const merger = new iCalMerge(old_iCalContent, new_iCalContent);
const merged_iCalContent = merger.merge();
```

## Methods

### merge(overwrite_days = true)

This method performs the merging of the two iCal files. By default, it overwrites events in the old iCal file that fall within the date range of events in the new iCal file. You can set the `overwrite_days` parameter to `false` if you want to keep the old events even if they overlap with the new ones.

```javascript
// Perform the merge with overwriting of overlapping events
merger.merge();

// Perform the merge without overwriting overlapping events
merger.merge(false);
```

## Contribution

Contributions to iCalMerge are welcome! If you find any issues or have suggestions for improvements, feel free to open an issue or submit a merge request on the [GitLab repository](https://gitlab.com/stefankoenders/ical-merge).

## License

iCalMerge is open-source software licensed under the [MIT License](https://gitlab.com/stefankoenders/ical-merge/-/blob/main/LICENSE). Feel free to use and modify it according to your needs.