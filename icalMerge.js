/**
 * iCalMerge
 * 
 * This class allows 2 iCal files(.ics) to be merged together.
 */
class iCalMerge {
    constructor(old_ics, new_ics) {
        this.old_ics = this.strip_ics(old_ics);
        this.new_ics = this.strip_ics(new_ics);
        this.old_events = [];
        this.new_events = [];
        this.merged_events = [];
    }

    toString() {
        return this.merged_events.join('\n');
    }

    get_new_date_range() {
        return this.get_event_date_range(this.new_events);
    }

    merge(overwrite_days = true) {
        this.old_events = this.get_events(this.old_ics);
        this.new_events = this.get_events(this.new_ics);

        const new_date_range = this.get_event_date_range(this.new_events);

        if (overwrite_days) {
            const filtered_old_events = this.filter_events(this.old_events, new_date_range);
            this.merged_events.push(...filtered_old_events);
        } else {
            this.merged_events.push(...this.old_events);
        }

        this.merged_events.push(...this.new_events);
    }

    /**
     * Returns events as array
     * @param {String} ics - The STRIPPED ics file.
     * @return {Array} Array with events
     */
    get_events(ics) {
        const result = [];
        ics.split('BEGIN:VEVENT').forEach(event => {
            if (event.trim() !== '') {
                result.push('BEGIN:VEVENT' + event);
            }
        });

        return result;
    }

    /**
     * Ignore events that are in the given date range
     * @param {Array} events with events outside date range
     * @param {Array} date_range with start and end date
     */
    filter_events(events, date_range) {
        const filtered_events = [];

        events.forEach(event => {
            const date = parseInt(event.split('DTSTART:')[1].substr(0, 8));
            if ((date < date_range.startdate) || (date > date_range.enddate)) {
                filtered_events.push(event);
            }
        });

        return filtered_events;
    }

    /**
     * @param {Array} events filled with ics events
     * @return {Array} Array with start and end date for events.
     */
    get_event_date_range(events) {
        const result = {
            startdate: 999999999,
            enddate: 0
        };

        events.forEach(event => {
            const date = parseInt(event.split('DTSTART:')[1].substr(0, 8));
            if (date < result.startdate) {
                result.startdate = date;
            }
            if (date > result.enddate) {
                result.enddate = date;
            }
        });

        return result;
    }

    strip_ics(ics) {
        ics = ics.replace(/BEGIN:VCALENDAR\n/, '');
        ics = ics.replace(/PRODID:.+\n/, '');
        ics = ics.replace(/VERSION:2.0\n/, '');
        ics = ics.replace(/END:VCALENDAR\n/, '');

        return ics;
    }
}

module.exports = iCalMerge;
